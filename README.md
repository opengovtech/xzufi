# libzufi
Simple library to parse XZuFi data.

This library is currently in early alpha state. Merge requests and feature requests welcome!

The XZuFi specification can be found [here](https://www.xrepository.de/api/xrepository/urn:xoev-de:fim:standard:xzufi_2.2.0:dokument:Spezifikation_XZuFi_2.2.0).

## License
Licensed under the [EUPL](./LICENSE.txt).
